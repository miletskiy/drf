# -*- makefile -*-
SHELL=/bin/bash
# Copyright (с) 2017.

# constants
PROJECT_NAME=snippets
BIND_TO=0.0.0.0
BIND_TO_LOCAL=192.168.1.104
BIND_PORT=8028
MANAGE=python manage.py
DJANGO_SETTINGS_MODULE=tutorial.settings

LOCALPATH := ./src
PYTHONPATH := $(LOCALPATH)/
PYTHON_BIN := $(VIRTUAL_ENV)/bin

showenv:
	@echo 'Environment:'
	@echo '-----------------------'
	@$(PYTHON_BIN)/python -c "import sys; print('sys.path:', sys.path)"
	@echo 'PYTHONPATH:' $(PYTHONPATH)
	@echo 'PROJECT:' $(PROJECT_NAME)
	@echo 'DJANGO_SETTINGS_MODULE:' $(DJANGO_SETTINGS_MODULE)
	@echo 'DJANGO_LOCAL_SETTINGS_MODULE:' $(DJANGO_LOCAL_SETTINGS_MODULE)
	@echo 'DJANGO_TEST_SETTINGS_MODULE:' $(DJANGO_TEST_SETTINGS_MODULE)

help:
	@cat README.md

# https://github.com/kaleissin/django-makefile/blob/master/Makefile


include

.PHONY: run open local clean manage help test flake8 shell ishell bshell hshell push github

run:
	@echo Starting $(PROJECT_NAME) with $(DJANGO_SETTINGS_MODULE)...
	$(MANAGE) runserver $(BIND_TO):$(BIND_PORT) --settings=$(DJANGO_SETTINGS_MODULE)

runserver:
	@echo Starting $(PROJECT_NAME) local net ...
	$(MANAGE) runserver $(BIND_TO_LOCAL):$(BIND_PORT) --settings=$(DJANGO_SETTINGS_MODULE)

open:
	@echo Opening $(PROJECT_NAME) ...
	open 'http://$(BIND_TO):$(BIND_PORT)'

clean:
	@echo Cleaning up...
	find ./tutorial | grep '\.pyc$$' | xargs -I {} rm {}
	find ./snippets | grep '\.pyc$$' | xargs -I {} rm {}
	@echo Done

shell:
	$(MANAGE) shell_plus --ipython

pip-install:
	pip install -r requirements.txt

pip-upgrade:
	pip install --upgrade pip
	pip install --upgrade wheel
	pip install --upgrade setuptools

makemigrations:
	$(MANAGE) makemigrations

migrate:
	$(MANAGE) migrate
