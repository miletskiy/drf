from django.apps import AppConfig


class SnippetsConfig(AppConfig):

    name = 'snippets'
    label = 'snippets'
    verbose_name = 'Snippets'
